import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import {Feed} from "./pages/Feed";
import {Playback} from "./pages/Playback";
import './App.css';
import {Login} from "./pages/Login";


const App = () => {
    return (
        <Router>
            <div className="App">
                <Switch>
                    <Route exact path='/login'>
                        <Login/>
                    </Route>

                    <Route exact path="/">
                        <Feed/>
                    </Route>
                    <Route path="/watch/:videoId">
                        <Playback/>
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
