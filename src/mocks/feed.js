export default [
    {
        id: 1,
        title: 'lofi hip hop radio - beats to relax/study to',
        preview: 'https://i.ytimg.com/vi/-FlxM_0S2lA/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLD3LYBuGK16w_z9pqiHCvxAKOQ_zw',
        author: 'Lofi Girl',
        comments: [
            {
                text: 'Song are really ruler of everything!',
                author: 'Tally Hall'
            },
            {
                text: "I'll remember this songs till the end of my life",
                author: 'Kentaro Miura'
            },
            {
                text: 'What a cute girl on the video',
                author: ''
            },

        ]
    },
    {
        id: 2,
        title: 'N A R U T O V I B E S',
        preview: 'https://i.ytimg.com/vi/zL1gMeoN8bI/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLAZl_GUWRpb6Q6TrWbfVIUidISpYg',
        author: 'AnimeVibe'
    },
    {
        id: 3,
        title: 'Chilling Souls - A Relaxing Mix from Souls series',
        preview: 'https://i.ytimg.com/vi/FMxj-zHfZbw/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLAQg9z_BGa_kDovORhOFt16wk4rGw',
        author: 'kiaktus'
    },
    {
        id: 4,
        title: 'Genshin Impact Lofi ✨ 1 Hour Lofi Hip Hop',
        preview: 'https://i.ytimg.com/vi/wofB1wzyYYI/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLAitSrbj3D3Y7gO_nlReojEU1vxeg',
        author: 'Leaflet'
    },
    {
        id: 5,
        title: '[ 𝑷𝒍𝒂𝒚𝒍𝒊𝒔𝒕 ] aesthetic song • lofi type beat • 3 hours',
        preview: 'https://i.ytimg.com/vi/juuUPOBenN4/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLBUp8ShcNhGD9sKLH3-DbwP4ii-5w',
        author: 'EYM'
    },
    {
        id: 6,
        title: 'Naruto Chill Trap, Lofi Hip Hop Mix',
        preview: 'https://i.ytimg.com/vi/jrTMMG0zJyI/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLAuZMlikrd4G3iClz6YSd5UFswXBA',
        author: 'yanu_연우'
    },
    {
        id: 7,
        title: 'Chill Study Beats 📚 ~ Lofi Hip Hop Mix',
        preview: 'https://i.ytimg.com/vi/FjHGZj2IjBk/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLBpOkQk_C8SpCwFpdHxrE5yMGKgrg',
        author: 'Lo-fi HiRes'
    },
    {
        id: 8,
        title: 'Food lover 🍜🍔 food lofi mix [chillhop mix]',
        preview: 'https://i.ytimg.com/vi/zRrYXvVOquQ/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLCqQztxBVNuVSwKDOqqYjkCZK4XBA',
        author: 'Chill Sesh Channel'
    },
    {
        id: 9,
        title: 'calvin harris - outside (slowed tik tok remix)',
        preview: 'https://i.ytimg.com/vi/BLjsPXeE2mw/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLAhUr0Q0KFAZbr0XTFKS7fhxfkN2g',
        author: 'ebony masters'
    },
    {
        id: 10,
        title: '10',
        preview: '',
        author: 'author_10'
    },
    {
        id: 11,
        title: '11',
        preview: '',
        author: 'author_11'
    },
    {
        id: 12,
        title: '12',
        preview: '',
        author: 'author_12'
    },
    {
        id: 13,
        title: '13',
        preview: '',
        author: 'author_13'
    },

]