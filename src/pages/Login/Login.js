import React, {useState} from "react";

export const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleLogin = () => {
        window.location.href = '/'
    }

    return (
        <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginTop: '30vh'}}>
            <input
                type='text'
                value={email}
                onChange={(val) => setEmail(val.target.value)}
                placeholder='Email'
                style={{
                    width: '15vw',
                    height: 45,
                    marginBottom: 30,
                    backgroundColor: 'rgba(0,0,0,0)',
                    color: '#b4b4b4',
                    border: '1px solid #b4b4b4',
                    paddingLeft: 10,
                    borderRadius: 10
                }}
            />
            <input
                type='password'
                value={password}
                onChange={(val) => setPassword(val.target.value)}
                placeholder='Password'
                style={{
                    width: '15vw',
                    height: 45,
                    marginBottom: 30,
                    backgroundColor: 'rgba(0,0,0,0)',
                    color: '#b4b4b4',
                    border: '1px solid #b4b4b4',
                    paddingLeft: 10,
                    borderRadius: 10
                }}
            />

            <div className={['button', 'main-text'].join(' ')} onClick={handleLogin}>Submit</div>

        </div>
    )
}