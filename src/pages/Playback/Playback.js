import React, {createRef, useState} from 'react'
import ReactPlayer from 'react-player/lazy'
import {useParams} from "react-router-dom";
import FEED_MOCK from '../../mocks/feed';


const CommentItem = (props) => {
    const {text, author} = props;

    return (
        <div className='main-text' style={{textAlign: 'left', marginTop: 20}}>
            <div style={{fontSize: 15, color: '#723f8c'}}>{author || 'Anonymous commenter'}</div>
            <div style={{fontSize: 20}}>{text}</div>
        </div>
    )
}

export const Playback = (props) => {
    const {} = props;
    const {videoId} = useParams();

    const currentVideo = FEED_MOCK.find(el => el.id === +videoId)
    const {title, author} = currentVideo;

    const [isLikePressed, setLikePressed] = useState(false);
    const [isDislikePressed, setDislikePressed] = useState(false);
    const [likesCount, setLikesCount] = useState(1826)
    const [dislikesCount, setDisikesCount] = useState(283)
    const [comments, setComments] = useState(currentVideo.comments || []);
    const [currentComment, setCurrentComment] = useState('');

    const handleLike = () => {
        setLikesCount(old => isLikePressed ? old - 1 : old + 1)
        setDisikesCount(old => isDislikePressed ? old - 1 : old)
        setLikePressed(!isLikePressed);
        setDislikePressed(false);
    }
    const handleDislike = () => {
        setDisikesCount(old => isDislikePressed ? old - 1 : old + 1)
        setLikesCount(old => isLikePressed ? old - 1 : old)
        setDislikePressed(!isDislikePressed);
        setLikePressed(false);
    }

    const handleComment = () => {
        setComments([{text: currentComment, author: ''}, ...comments])
        setCurrentComment('')
    }


    return (
        <div style={{display: 'flex', flexDirection: 'row'}}>
            <div style={{width: '60vw', margin: '5vh auto'}}>
                <ReactPlayer
                    url='https://www.youtube.com/watch?v=5qap5aO4i9A&ab_channel=LofiGirl'
                    width={'60vw'}
                    height={'68vh'}
                    controls={true}
                    playing={true}
                />
                <div className='main-text'
                     style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                    <div style={{textAlign: 'left', paddingLeft: 15}}>
                        <div style={{fontSize: 28, paddingTop: '10px'}}>{title}</div>
                        <div>{author}</div>
                    </div>

                    <div style={{display: 'flex', flexDirection: 'row', margin: '30px 20px'}}>
                        <div
                            className={isLikePressed ? 'clickable-active' : 'clickable'}
                            onClick={handleLike}
                        >
                            👍 {likesCount}
                        </div>
                        <div
                            className={isDislikePressed ? 'clickable-active' : 'clickable'}
                            onClick={handleDislike}
                            style={{marginLeft: 10}}
                        >
                            👎 {dislikesCount}
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div style={{display: 'flex', flexDirection: 'row', marginTop: 40, marginRight: 15}}>
                    <input type='text' value={currentComment} onChange={(val) => setCurrentComment(val.target.value)}
                           placeholder='Write a comment...'
                       style={{
                        width: '25vw',
                        height: 45,
                        marginRight: 30,
                        backgroundColor: 'rgba(0,0,0,0)',
                        color: '#b4b4b4',
                        border: '1px solid #b4b4b4',
                        paddingLeft: 10
                    }}/>
                    <div className={['button', 'main-text'].join(' ')} onClick={handleComment}>Submit</div>
                </div>

                <div style={{maxHeight: '70vh', width: '33.5vw', overflowY: 'auto', marginTop: 5}}>
                    {comments.map(el => <CommentItem text={el.text} author={el.author}/>)}
                </div>
            </div>
        </div>
    )
}