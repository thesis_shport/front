import placeholderImage from '../../assets/video-placeholder-gray.png';
import FEED_MOCK from '../../mocks/feed';
import {useState} from "react";


const FeedItem = (props) => {
    const {id, title, author, preview} = props;

    console.log(preview, placeholderImage, preview ? preview : `url('../../assets/video-placeholder-gray.png')`);

    return (
        <div style={{margin: '15px', textAlign: 'left', cursor: 'pointer'}} onClick={() => window.location = `/watch/${id}`}>
            <img style={{width: '365px', height: '205px', border: '1px solid #A9A9A9'}} src={preview ? preview : placeholderImage}/>
            <div className='main-text' style={{paddingLeft: 10, paddingTop: 5}}>{title.length>40 ? title.slice(0,40)+'...' : title}</div>
            <div style={{fontSize: 16, color: '#747474', paddingLeft: 10}}>{author}</div>
        </div>
    )
}

export const Feed = (props) => {
    const {} = props;

    const [isExtended, setExtended] = useState(false);

    const videosToShow = isExtended ? FEED_MOCK : FEED_MOCK.slice(0, 8)

    return  (
        <div style={{width: '1600px', margin: '30px auto'}}>
            <div className='main-text' style={{marginTop: 40, marginBottom: 15, fontSize: 24, textAlign: 'left', fontWeight: 600}}>Popular now</div>
            <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center',
                alignItems: 'center', alignContent: 'flex-start'}}>
            {
                videosToShow.map(({id, title, author, preview}) => <FeedItem id={id} title={title} author={author} preview={preview}/>)
            }
            {!isExtended && <div className={['button', 'main-text'].join(' ')} onClick={setExtended}>Show more</div>}
            </div>
        </div>
    )
}